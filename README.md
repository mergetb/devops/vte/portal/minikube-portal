
# Overview
A portal based on Minikube, Helm, and Ansible. Minikube builds the Kubernetes cluster, while Ansible and portal Helm charts install and configure the Merge Portal.

This process creates a VM, installs the portal, and sets up the `portalops`, `testuser`, and `tbops` accounts. A simple set of four nodes is added as resources (these nodes are not real). Multiple instances can be run by invoking `start-portal` with `$NAME` defined in the environment.

---

# Setup

1. **Connect to a node in your experiment using your XDC environment**:
    ```bash
    mrg xdc ssh -x <xdc-name> <node-name>
    ```

2. **Clone the repository on the node**:

    Using HTTPS:
    ```bash
    git clone https://gitlab.com/mergetb/devops/vte/portal/minikube-portal.git
    ```

    Using SSH:
    ```bash
    git clone git@gitlab.com:mergetb/devops/vte/portal/minikube-portal.git
    ```

3. **Navigate to the `host` directory in the repository and modify the following files**:

    a. `inventory`:
    - Replace the existing experiment node name with your own:
    - Run `hostname` in the terminal and copy the result into this file

    b. `extra-vars.yml`:
    - Replace the existing list of users with the list of users working on your project:
        - For a **single** user:
           - Replace the existing names with your own in the `users` section.
        - For **multiple** users:
            - Run the following command and paste the output into the `users` section:
             ```bash
             mrg show project $PROJECT -j | jq -r '.project.members | keys[]'
             ```

4. **Install Ansible**:
    ```bash
    sudo apt install ansible
    ``` 

5. **Run the Ansible playbook**:
    ```bash
    ansible-playbook -i inventory -e @extra-vars.yml portal.yml
    ```
    If you get the following error:
    > "Failed to set permissions on the temporary files Ansible needs to create when becoming an unprivileged user"

    Run the command below, then re-run the Ansible playbook:
    ```bash
    sudo apt-get install acl
    ```

6. **Move out of the `host` directory and into the main directory**.

7. **Start the portal**:
    ```bash
    ./start-portal
    ```
    If you get the following error:
    > Exiting due to PR_KVM_USER_PERMISSION: libvirt group membership check failed: user is not a member of the appropriate libvirt group

    Run the following:
    ```bash
    sudo usermod -aG libvirt $(whoami)
    ```    
    Then exit and re-launch the shell for the changes to take effect.
    To confirm the changes are applied, run:
    ```bash
    groups
    ```


    If you get the following error:
    > mkdir /home/\<user-name>/.config/helm: permission denied
    Fix the permissions:
    ```bash
    sudo chown -R <user-name>:<user-name> /home/<user-name>/.config
    ```

---
# Clean-up
To stop the portal, run:
```bash
./delete-portal
```
---
# Things that aren't working
1) SSH jump network glue is not configured.
2) No working WireGuard.
3) A disconnected Phobos instance with four nodes is set up as a resource for realization.
4) No materialization since there is no actual testbed.



