#!/usr/bin/env bash

mrg config set server grpc.$PORTAL_FQDN

opsid=$(helm -n merge get values portal -o json | jq -r .opsId)
opspw=$(helm -n merge get values portal -o json | jq -r .opsPw)

userpw=${USERPW:-'k4tBack!'}

#  create test user and tb ops user.
mrg register testuser testuser@example.net 'Test User' ISI OPS USA -p $userpw
mrg register tbops tbops@example.net 'Testbed Ops' ISI OPS USA -p $userpw

mrg login $opsid --nokeys -p $opspw --nokeys

mrg init testuser
mrg activate user testuser

mrg init tbops
mrg activate user tbops

# init pools as test user
mrg login testuser -p $userpw
mrg new pool default

# add the facility and the nodes to the pool
mrg login tbops -p $userpw
mrg new facility phobos api.phobos.example.com testbed/phobos/tb.xir testbed/phobos/portal.pem
mrg pool add facility default phobos x0 x1 x2 x3

mrg logout

# add the valid categories(for organization/projects)
mrg new portal entity-type Research DDoS DNS Evaluation Spoofing CyberSecurity
mrg new portal entity-type Class

